<?php

/**
 * Файл login.php для не авторизованного пользователя выводит форму логина.
 * При отправке формы проверяет логин/пароль и создает сессию,
 * записывает в нее логин и id пользователя.
 * После авторизации пользователь перенаправляется на главную страницу
 * для изменения ранее введенных данных.
 *
 */
 
// Отправляем браузеру правильную кодировку,
// файл login.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');

// Начинаем сессию.
session_start();

// В суперглобальном массиве $_SESSION хранятся переменные сессии.
// Будем сохранять туда логин после успешной авторизации.

if (!empty($_SESSION['login'])) {
    // Если есть логин в сессии, то пользователь уже авторизован.
    // TODO: Сделать выход (окончание сессии вызовом session_destroy()
    //при нажатии на кнопку Выход).
    // Делаем перенаправление на форму.
    //session_destroy();
    header('Location: ./');
}

// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
?>


<?php
    if (!empty($_COOKIE['open_error'])) {
        echo '<div class = "error"> Неверный логин и/или пароль!</div>';
        setcookie('open_error', '', 1000);
    }
?>

<!DOCTYPE html>
<html lang="ru">
   <head>
      <style>
         /* Сообщения об ошибках и поля с ошибками выводим с красным бордюром. */
         .error {
         border: 5px outset red;
         }
      </style>
      <meta charset="utf-8">
      <title>Lab5</title>
      <link rel="stylesheet" href="style.css">
   </head>
   <body>
      <div class="col-12 order-0 order-md-1">
         <h2 id="head3" style="text-align: center">Вход</h2>
         <form class="form-horizontal my_form" action="" method="POST">
            <label> Логин:
            <br>
            <input name="login" />
            </label>
            <br>
            <label> Пароль:
            <br>
            <input name="pass" />
            </label>
            <br>
            <br>
            <div class="form-group">
               <input type="submit" class="button" value="ВОЙТИ" />
            </div>
            <br>
         </form>
      </div>
   </body>
</html>

<?php
}

// Иначе, если запрос был методом POST, т.е. нужно сделать авторизацию с записью логина в сессию.
else {
    
    // Проверяем, есть ли такой логин и пароль в базе данных.
    // Выдаем сообщение об ошибках.
    
    $user = 'u15645';
    $mypass = '3395578';
    $db = new PDO('mysql:host=localhost;dbname=u15645', $user, $mypass, array(PDO::ATTR_PERSISTENT => true));
    // Подготовленный запрос.
    try {
        $stmt = $db->prepare("SELECT * FROM forma_lab5 WHERE login = ? AND pass_md5 = ?;");
        $stmt->bindValue(1, $_POST['login'], PDO::PARAM_STR);
        $stmt->bindValue(2, md5($_POST['pass']), PDO::PARAM_STR);
        $stmt->execute();
    }
    catch(PDOException $e) {
        print ('Error : ' . $e->getMessage());
        exit();
    }
    
    $user_id = $stmt->fetchAll(PDO::FETCH_ASSOC);   //массив, в к-ый сохраняются все полученные строки из БД.
    
    if (!empty($user_id)) {
        // Если все ок, то авторизуем пользователя.
        $_SESSION['login'] = $_POST['login'];
        $_SESSION['pass'] = $_POST['pass'];
        // Записываем ID пользователя, взятый из БД.
        $_SESSION['uid'] = $user_id[0]['id'];
        
        // Делаем перенаправление.
        header('Location: ./');
    } else {
        setcookie('open_error', 1, 0);    //ошибка о вводе неверного пароля/логина.
        header('Location: login.php');
    }
}
