<!DOCTYPE html>
<html lang="ru">
   <head>
      <style>
         /* Сообщения об ошибках и поля с ошибками выводим с красным бордюром. */
         .error {
         border: 5px outset red;
         }
      </style>
      <meta charset="utf-8">
      <title>Lab5</title>
      <link rel="stylesheet" href="style.css">
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
      <meta name="viewport" content="width=device-width, initial-scale=1">
   </head>
   <body>
      <?php
         if (!empty($messages)) {
           print('<div id="messages">');
           // Выводим все сообщения.
           foreach ($messages as $message) {
             print($message);
           }
           print('</div>');
         }
         
         // Далее выводим форму отмечая элементы с ошибками классом error
         // и задавая начальные значения элементов ранее сохраненными.
         ?>
      <div class="col-12 order-0 order-md-1">
         <h2 id="head3" style="text-align: center"><u>Форма</u></h2>
         <br>
         <form class="form-horizontal my_form" action="index.php" method="POST">
            <label> ФИО:
            <br>
            <input name="fio" <?php if ($errors['fio']) {print 'class="error"';} ?> value="<?php print $values['fio']; ?>" />
            </label>
            <br>
            <p>
               <label> Email:
               <br>
               <input name="email" <?php if ($errors['email']) {print 'class="error"';} ?> value="<?php print $values['email']; ?>" type="email">
               </label>
            </p>
            <p>
               <label> Дата рождения:
               <br>
               <input name="date" <?php if ($errors['date']) {print 'class="error"';} ?> value="<?php print $values['date']; ?>" type="date">
               </label>
            </p>
            <div <?php if ($errors['gender']) {print 'class="error"';} ?>>
               <div class="radio">
                  <label> Пол:
                  <br>
                  <input type="radio" name="gender" id="g1" value="Man" <?php if($values['gender'] == "Man") {print 'checked';} ?>> Мужской
                  </label>
               </div>
               <br>
               <div class="radio">
                  <label>
                  <input type="radio" name="gender" id="g2" value="Woman" <?php if($values['gender'] == "Woman") {print 'checked';} ?>> Женский
                  </label>
               </div>
            </div>
            <br>
            <div <?php if ($errors['kolvo']) {print 'class="error"';} ?>>
               <div class="radio">
                  <label> Количество конечностей:
                  <br>
                  <input type="radio" name="kolvo" id="k1" value="1" <?php if($values['kolvo'] == "1") {print 'checked';} ?>> 1
                  </label>
               </div>
               <div class="radio">
                  <label>
                  <input type="radio" name="kolvo" id="k2" value="2" <?php if($values['kolvo'] == "2") {print 'checked';} ?>> 2
                  </label>
               </div>
               <div class="radio">
                  <label>
                  <input type="radio" name="kolvo" id="k3" value="3" <?php if($values['kolvo'] == "3") {print 'checked';} ?>> 3
                  </label>
               </div>
               <div class="radio">
                  <label>
                  <input type="radio" name="kolvo" id="k4" value="4" <?php if($values['kolvo'] == "4") {print 'checked';} ?>> 4
                  </label>
               </div>
            </div>
            <br>
            <div <?php if (!empty($errors['superpower'])) {print 'class="error"';} ?>>
               <label>
               Сверхспособности:
               <br>
               <select name="superpower[]" size=3 multiple="multiple">
               <?php
                  foreach ($superpower as $key => $value) {
                    $selected = empty($values['superpower'][$key]) ? '' : ' selected="selected"';
                    printf('<option value="%s"%s>%s</option>', $key, $selected, $value);
                  }
                  ?>
               </select>
               </label>
            </div>
            <br>
            <label> Биография:
            <br>
            <input type="text" name="bio" style="width: 250px; height: 160px;" <?php if ($errors['bio']) {print 'class="error"';} ?> value="<?php print $values['bio']; ?>">
            </label>
            <br>
            <div <?php if ($errors['contract']) {print 'class="error"';} ?>>
               <div class="checkbox">
                  <label>
                  <input type="checkbox" name="contract" <?php if($values['contract'] == "on") {print 'checked';}?>> С контрактом ознакомлен(а)
                  </label>
               </div>
            </div>
            <div class="form-group">
               <input type="submit" class="button" value="SEND" />
            </div>
         </form>
      </div>
   </body>
</html>