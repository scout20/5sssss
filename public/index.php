<?php

/**
 * Реализовать возможность входа с паролем и логином с использованием
 * сессии для изменения отправленных данных в предыдущей задаче,
 * пароль и логин генерируются автоматически при первоначальной отправке формы.
 */
 
// Отправляем браузеру правильную кодировку,
// файл index.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');

$superpower = ['Immortality' => 'Бессмертие', 'Passing_through_walls' => 'Прохождение сквозь стены', 'Levitation' => 'Левитация']; //массив для формы

// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    
    // Массив для временного хранения сообщений пользователю.
    $messages = array();
    // В суперглобальном массиве $_COOKIE PHP хранит все имена и значения куки текущего запроса.
    // Выдаем сообщение об успешном сохранении.
    if (!empty($_COOKIE['save'])) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('save', '', 100000);
        setcookie('login', '', 100000);
        setcookie('pass', '', 100000);
        // Выводим сообщение пользователю.
        $messages[] = 'Спасибо, Ваши данные сохранены!';
        // Если в куках есть пароль, то выводим сообщение.
        if (!empty($_COOKIE['pass'])) {
            $messages[] = sprintf(' Вы можете <a href="login.php">войти</a> с логином <strong>%s</strong> и паролем <strong>%s</strong> для изменения своих данных.', strip_tags($_COOKIE['login']), strip_tags($_COOKIE['pass']));
        }
    }
    
    // Складываем признак ошибок в массив.
    $errors = array();
    $errors['fio'] = (!empty($_COOKIE['fio_error']));
    $errors['email'] = (!empty($_COOKIE['email_error']));
    $errors['date'] = (!empty($_COOKIE['date_error']));
    $errors['gender'] = (!empty($_COOKIE['gender_error']));
    $errors['kolvo'] = (!empty($_COOKIE['kolvo_error']));
    $errors['superpower'] = (!empty($_COOKIE['superpower_error']));
    $errors['bio'] = (!empty($_COOKIE['bio_error']));
    $errors['contract'] = (!empty($_COOKIE['contract_error']));
    
    // Выдаем сообщения об ошибках.
    if ($errors['fio']) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('fio_error', '', 100000);
        // Выводим сообщение.
        $messages[] = '<div class="error">Поле "ФИО" заполнено неправильно.</div>';
    }
    if ($errors['email']) {
        setcookie('email_error', '', 100000);
        $messages[] = '<div class="error">Поле "Email" заполнено неправильно.</div>';
    }
    if ($errors['date']) {
        setcookie('date_error', '', 100000);
        $messages[] = '<div class="error">Поле "Дата рождения" заполнено неправильно.</div>';
    }
    if ($errors['gender']) {
        setcookie('gender_error', '', 100000);
        $messages[] = '<div class="error">Пол не выбран.</div>';
    }
    if ($errors['kolvo']) {
        setcookie('kolvo_error', '', 100000);
        $messages[] = '<div class="error">Количество конечностей не выбрано.</div>';
    }
    if ($errors['superpower']) {
        setcookie('superpower_error', '', 100000);
        $messages[] = '<div class="error">Сверхспособности не выбраны.</div>';
    }
    if ($errors['bio']) {
        setcookie('bio_error', '', 100000);
        $messages[] = '<div class="error">Поле "Биография" заполнено неправильно.</div>';
    }
    if ($errors['contract']) {
        setcookie('contract_error', '', 100000);
        $messages[] = '<div class="error">Ознакомьтесь с контрактом.</div>';
    }
    
    
    // Складываем предыдущие значения полей в массив, если есть.
    $values = array();
    $values['fio'] = (empty($_COOKIE['fio_value']) || !preg_match('/^[а-яА-ЯёЁa-zA-Z ]+$/u', $_COOKIE['fio_value'])) ? '' : strip_tags($_COOKIE['fio_value']);
    $values['email'] = (empty($_COOKIE['email_value']) || !preg_match('/^([A-Za-z0-9_\-\.])+@([A-Za-z0-9_\-\.])+\.([A-Za-z0-9])+$/', $_COOKIE['email_value']) ? '' : $_COOKIE['email_value']);
    $values['date'] = (empty($_COOKIE['date_value']) || !preg_match('/[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])/u', $_COOKIE['date_value'])) ? '' : $_COOKIE['date_value'];
    $values['gender'] = (empty($_COOKIE['gender_value']) ? '' : $_COOKIE['gender_value']);
    $values['kolvo'] = (empty($_COOKIE['kolvo_value']) ? '' : $_COOKIE['kolvo_value']);
    $values['bio'] = (empty($_COOKIE['bio_value']) ? '' : $_COOKIE['bio_value']);
    $values['contract'] = (empty($_COOKIE['contract_value']) ? ' ' : $_COOKIE['contract_value']);
    // Обработка способностей из Cookies:
    $values['superpower'] = [];
    if (!empty($_COOKIE['superpower_value'])) {
        $superpower_cookies = json_decode($_COOKIE['superpower_value']);
        if (is_array($superpower_cookies)) {
            foreach ($superpower_cookies as $ability) {
                if (!empty($superpower[$ability])) {
                    $values['superpower'][$ability] = $ability;
                }
            }
        }
    }
    
    // Если нет предыдущих ошибок ввода, есть кука сессии, начали сессию и ранее в сессию записан факт успешного логина:
    if (!in_array(1, $errors) && !empty($_COOKIE[session_name() ]) && session_start() && !empty($_SESSION['login'])) {
        
        // Загружаем данные пользователя из БД
        // и заполняем переменную $values.
        $pass_md5 = md5($_SESSION['pass']);    //пароль пользователя (хэш).
        
        $user = 'u15645';
        $mypass = '3395578';
        $db = new PDO('mysql:host=localhost;dbname=u15645', $user, $mypass, array(PDO::ATTR_PERSISTENT => true));
        // Подготовленный запрос.
        try {
            $stmt = $db->prepare('SELECT name, email, date, gender, kolvo, superpower, bio, contract FROM forma_lab5 WHERE login = ? AND pass_md5 = ?;');
            $stmt->bindValue(1, $_SESSION['login'], PDO::PARAM_STR);
            $stmt->bindValue(2, $pass_md5, PDO::PARAM_STR);
            $stmt->execute();
        }
        catch(PDOException $e) {
            print ('Error : ' . $e->getMessage());
            exit();
        }
        
        $user_data = $stmt->fetchAll(PDO::FETCH_ASSOC) [0];
        $values['fio'] = !empty($user_data['name']) ? $user_data['name'] : '';
        $values['email'] = !empty($user_data['email']) ? $user_data['email'] : '';
        $values['date'] = !empty($user_data['date']) ? $user_data['date'] : '';
        $values['gender'] = !empty($user_data['gender']) ? $user_data['gender'] : '';
        $values['kolvo'] = !empty($user_data['kolvo']) ? $user_data['kolvo'] : '';
        $values['bio'] = !empty($user_data['bio']) ? $user_data['bio'] : '';
        $values['contract'] = !empty($user_data['contract']) ? 'on' : '';
        $values['superpower'] = [];
        if (!empty($user_data['superpower'])) {
            $superpower_data = json_decode($user_data['superpower']);
            if (is_array($superpower_data)) foreach ($superpower_data as $ability) {
                if (!empty($superpower[$ability])) {
                    $values['superpower'][$ability] = $ability;
                }
            }
        }
        printf(' Выполнен вход с логином %s. Ваш id: %d.', $_SESSION['login'], $_SESSION['uid']);
    }
    
    // Включаем содержимое файла form.php.
    // В нем будут доступны переменные $messages, $errors и $values для вывода
    // сообщений, полей с ранее заполненными данными и признаками ошибок.
    include ('form.php');
}

// Иначе, если запрос был методом POST, т.е. нужно проверить данные и сохранить их в XML-файл.
else {
    
    // Проверяем ошибки.
    $errors = false;
    if (!(preg_match('/^[а-яА-ЯёЁa-zA-Z ]+$/u', $_POST['fio']))) {
        setcookie('fio_error', '1', 0);
        $errors = true;
    } else {
        // Сохраняем ранее введенное в форму значение на год.
        setcookie('fio_value', $_POST['fio'], time() + 12 * 30 * 24 * 60 * 60);
    }
    if (!(preg_match('/^([A-Za-z0-9_\-\.])+@([A-Za-z0-9_\-\.])+\.([A-Za-z0-9])+$/', $_POST['email']))) {
        setcookie('email_error', '1', 0);
        $errors = true;
    } else {
        setcookie('email_value', $_POST['email'], time() + 12 * 30 * 24 * 60 * 60);
    }
    if (!(preg_match('/^\d\d\d\d\-\d\d\-\d\d$/', $_POST['date']))) {
        setcookie('date_error', '1', 0);
        $errors = true;
    } else {
        setcookie('date_value', $_POST['date'], time() + 12 * 30 * 24 * 60 * 60);
    }
    if (empty($_POST['gender'])) {
        $_POST['gender'] = '';
        setcookie('gender_error', '1', 0);
        $errors = true;
    } else {
        setcookie('gender_value', $_POST['gender'], time() + 12 * 30 * 24 * 60 * 60);
    }
    if (empty($_POST['kolvo'])) {
        $_POST['kolvo'] = '';
        setcookie('kolvo_error', '1', 0);
        $errors = true;
    } else {
        setcookie('kolvo_value', $_POST['kolvo'], time() + 12 * 30 * 24 * 60 * 60);
    }
    if (empty($_POST['superpower'])) {
        setcookie('superpower_error', '1', 0);
        $errors = true;
    } else {
        foreach ($_POST['superpower'] as $ability) {
            if (empty($superpower[$ability])) {
                print ('Недопустимая способность.');
                exit();
            }
        }
        setcookie('superpower_value', json_encode($_POST['superpower']), time() + 12 * 30 * 24 * 60 * 60);
    }
    if (empty($_POST['bio'])) {
        setcookie('bio_error', '1', 0);
        $errors = true;
    } else {
        setcookie('bio_value', $_POST['bio'], time() + 12 * 30 * 24 * 60 * 60);
    }
    if (empty($_POST['contract'])) {
        setcookie('contract_error', '1', 0);
        $errors = true;
    } else {
        setcookie('contract_value', $_POST['contract'], time() + 12 * 30 * 24 * 60 * 60);
    }
    
    if ($errors) {
        // При наличии ошибок перезагружаем страницу и завершаем работу скрипта.
        header('Location: index.php');
        exit();
    } else {
        // Удаляем Cookies с признаками ошибок.
        setcookie('fio_error', '', 100000);
        setcookie('email_error', '', 100000);
        setcookie('date_error', '', 100000);
        setcookie('gender_error', '', 100000);
        setcookie('kolvo_error', '', 100000);
        setcookie('superpower_error', '', 100000);
        setcookie('bio_error', '', 100000);
        setcookie('contract_error', '', 100000);
    }
    
    // Сохраняем куку с признаком успешного сохранения.
    setcookie('save', '1');
    
    // Проверяем, меняются ли ранее сохраненные данные или отправляются новые.
    if (!empty($_COOKIE[session_name() ]) && session_start() && !empty($_SESSION['login'])) {
        
        // Перезаписываем данные в БД новыми данными.
        $user = 'u15645';
        $mypass = '3395578';
        $db = new PDO('mysql:host=localhost;dbname=u15645', $user, $mypass, array(PDO::ATTR_PERSISTENT => true));
        // Подготовленный запрос.
        try {
            $stmt = $db->prepare("UPDATE forma_lab5 SET name = ?, email = ?, date = ?, gender = ?, kolvo = ?, superpower = ?, bio = ?, contract = ? WHERE id = ?;");
            $stmt->execute(array($_POST['fio'], $_POST['email'], $_POST['date'], $_POST['gender'], $_POST['kolvo'], json_encode($_POST['superpower']), $_POST['bio'], 1, $_SESSION['uid']));
        }
        catch(PDOException $e) {
            print ('Error : ' . $e->getMessage());
            exit();
        }
    } else {
        
        // Генерируем уникальный логин и пароль.
        // Механизм генерации ( функциями rand(), uniquid(), md5(), substr() ).
        
        $login = $_POST['email'];
        $login.= '_log';
        $pass = substr(uniqid(), 0, 8);
        // Сохраняем в Cookies.
        setcookie('login', $login);
        setcookie('pass', $pass);
        
        // Сохранение данных формы, логина и хеш md5() пароля в базу данных.
        $user = 'u15645';
        $mypass = '3395578';
        $db = new PDO('mysql:host=localhost;dbname=u15645', $user, $mypass, array(PDO::ATTR_PERSISTENT => true));
        // Подготовленный запрос.
        try {
            $stmt = $db->prepare("INSERT INTO forma_lab5 (name, email, login, pass_md5, date, gender, kolvo, superpower, bio, contract) VALUES (:name, :email, :login, :pass_md5, :date, :gender, :kolvo, :superpower, :bio, :contract);");
            $stmt->bindParam(':name', $name);
            $stmt->bindParam(':email', $email);
            $stmt->bindParam(':login', $login);
            $stmt->bindParam(':pass_md5', $pass_md5);
            $stmt->bindParam(':date', $date);
            $stmt->bindParam(':gender', $gender);
            $stmt->bindParam(':kolvo', $kolvo);
            $stmt->bindParam(':superpower', $superpower);
            $stmt->bindParam(':bio', $bio);
            $stmt->bindParam(':contract', $contract);
            $name = $_POST['fio'];
            $email = $_POST['email'];
            $pass_md5 = md5($pass);
            $date = $_POST['date'];
            $gender = $_POST['gender'];
            $kolvo = $_POST['kolvo'];
            $superpower = json_encode($_POST['superpower']);
            $bio = $_POST['bio'];
            $contract = $_POST['contract'];
            if ($contract == "on") {
                $contract = 1;
            } else {
                $contract = 0;
            }
            $stmt->execute();
        }
        catch(PDOException $e) {
            print ('Error : ' . $e->getMessage());
            exit();
        }
    }
    
    // Сохраняем куку с признаком успешного сохранения.
    setcookie('save', '1');
    // Делаем перенаправление.
    header('Location: ./');
}
